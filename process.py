#!/usr/bin/env python
# coding: utf-8

import os
import sys
import json
import PIL
import numpy
import torch
import torchvision
from torchvision import transforms


if torch.cuda.is_available():
    device = torch.device("cuda") 
else:
    device = torch.device("cpu")


idx_to_class = ["female", "male"]

path = sys.argv[1]

img_size = 256
num_workers = 10
batch_size = 32

transform = transforms.Compose([
    transforms.RandomResizedCrop(img_size),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406],
                         [0.229, 0.224, 0.225])
])

class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, path, transform):
        self.img_names = os.listdir(path)
        self.path = path
        self.transform = transform

    def __len__(self):
        return len(self.img_names)

    def __getitem__(self, idx):
        img_name = self.img_names[idx]
        img = PIL.Image.open(os.path.join(path, img_name))

        return transform(img), img_name


dataset = CustomDataset(path, transform)

batch_gen = torch.utils.data.DataLoader(dataset, 
                                              batch_size=batch_size,
                                              shuffle=False,
                                              num_workers=num_workers)

model = torch.load("model.pt")

result = {}

for X_batch, img_names in batch_gen:
    logits = model(X_batch.to(device))
    y_pred = logits.max(1)[1].cpu().data.numpy().tolist()
    result.update(dict(zip(img_names, map(lambda x: idx_to_class[x], y_pred))))

with open("process_results.json", "w") as f:
    json.dump(result, f)

